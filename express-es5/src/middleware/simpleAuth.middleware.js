const response = require('../../helper/response')

function simpleAuth(req, res, next) {
    try {
        const token = req.header('Authorization').split('Bearer ')
        if (token[1] === '12345678asdf') {
            return next()
        } else {
            response.failed(res, 401,  "Unauthorized.")
        }
    } catch(error) {
        console.log(console.log(error))
        response.errorInternal(res)
    }
}

module.exports = simpleAuth