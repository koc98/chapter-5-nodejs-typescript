const express = require('express');
const app = express();
const bodyParser = require('body-parser')


require('dotenv').config()

const routes = require('./routes')
const response = require('../../helper/response')

app.use(bodyParser.json())
app.use(routes)

app.get('/ping', (req, res) => {
    response.success(res, 200, null, null, "Pong")
});

module.exports = app