const express = require('express')
const middleware = require('../middleware')
const apiRouter = require('../api')

const router = express.Router()


router.use('/v1/book', middleware.simpleAuth, apiRouter.bookRouter)
router.use('/v1/document', apiRouter.documentRouter)
router.use('/v1/user', apiRouter.userRouter)

module.exports = router
