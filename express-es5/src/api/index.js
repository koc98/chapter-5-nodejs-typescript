const bookRouter = require('./book')
const documentRouter = require('./document')
const userRouter = require('./user')

module.exports = {
    bookRouter,
    documentRouter,
    userRouter
}