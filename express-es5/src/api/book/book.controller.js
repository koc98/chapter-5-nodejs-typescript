const axios = require('axios')
const response = require('../../../helper/response')
class BookController {
    async getAllBooks(req, res) {
        try {
            axios.get(`${process.env.BOOK_URL}/book/list?key=${process.env.BOOK_KEY}`)
                .then(function (response) {
                    return response.data
                })
                .catch(function (error) {
                    console.log(error)
                    response.errorInternal(res)
                })
                .then(function (data) {
                    response.success(res, 200, data.meta, data.data, "Success Get All Books.")
                });
        } catch (error) {
            console.log(error)
            response.errorInternal(res)
        }
    }

    async getBook(req, res) {
        try {
            axios.get(`${process.env.BOOK_URL}/book/detail/${req.params.book_id}?key=${process.env.BOOK_KEY}`)
                .then(function (response) {
                    return response.data
                })
                .catch(function (error) {
                    console.log(error)
                    response.errorInternal(res)
                })
                .then(function (data) {
                    response.success(res, 200, data.meta, data.data, "Success Get a Book.")
                });
        } catch (error) {
            console.log(error)
            response.errorInternal(res)
        }
    }

    async createBook(req, res) {
        try {
            axios.post(`${process.env.BOOK_URL}/book/create?key=${process.env.BOOK_KEY}`, req.body)
                .then(function (response) {
                    return response.data
                })
                .catch(function (error) {
                    console.log(error)
                    response.errorInternal(res)
                })
                .then(function (data) {
                    response.success(res, 200, null, data.data, "Success Create a Book.")
                });
        } catch (error) {
            console.log(error)
            response.errorInternal(res)
        }
    }

    async deleteBook(req, res) {
        try {
            axios.get(`${process.env.BOOK_URL}/book/delete/${req.params.book_id}?key=${process.env.BOOK_KEY}`)
                .then(function (response) {
                    return response.data
                })
                .catch(function (error) {
                    console.log(error)
                    response.errorInternal(res)
                })
                .then(function (data) {
                    response.success(res, 200, data.meta, data.data, "Success Delete a Book.")
                });
        } catch (error) {
            console.log(error)
            response.errorInternal(res)
        }
    }


}

module.exports = BookController